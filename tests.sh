./buildjpg2pmc.sh
./buildrawpmc.sh
./buildvideopmc.sh
./jpg2pmc TestImages/Lenna.png  0204 TestImages/Lenna0204.pmc
./jpg2pmc TestImages/Lenna.png  0408 TestImages/Lenna0408.pmc
./jpg2pmc TestImages/Lenna.png  0612 TestImages/Lenna0612.pmc
./jpg2pmc TestImages/Lenna.png  1016 TestImages/Lenna1016.pmc
./jpg2pmc TestImages/Lenna.png  1632 TestImages/Lenna1632.pmc

./jpg2pmc TestImages/Lenna0204.pmc TestImages/Lenna0204.png
./jpg2pmc TestImages/Lenna0408.pmc TestImages/Lenna0408.png
./jpg2pmc TestImages/Lenna0612.pmc TestImages/Lenna0612.png
./jpg2pmc TestImages/Lenna1016.pmc TestImages/Lenna1016.png
./jpg2pmc TestImages/Lenna1632.pmc TestImages/Lenna1632.png

if [[ "$OSTYPE" == "linux-gnu" ]]; then
cp jpg2pmc jpg2pmcUbuntuLinux
cp videopmc videopmcUbuntuLinux
cp rawpmc rawpmcUbuntuLinux
elif [[ "$OSTYPE" == "darwin"* ]]; then
cp jpg2pmc jpg2pmcOSX
cp videopmc videopmcOSX
cp rawpmc rawpmcOSX
fi
