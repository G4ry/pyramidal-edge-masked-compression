#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <iostream>
#include <string>
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "pmc.h"

int main(int argc, char** argv)
{
    printf("jpg2pmc  G.Symons 2015\n");
    printf("Usage:\n");
    printf("To convert jpg file to pmc:    jpg2pmc file.jpg  [Thresholds concatenated 01-09's] file.pmc\n");
    printf("To convert pmc file to jpg:    jpg2pmc file.pmc file.jpg\n");
    printf("Examples:\n");
    printf("jpg2pmc test.jpg 010203 test.pmc    will use a 3 level pyramid with thresholds 1 for lowest res in pyramid, 2 for mid, and 3 for highest.\n");
    printf("jpg2pmc test.pmc test.jpg   will convert it back.\n");
    printf("Although called jpg2pmc it converts other extensions such as .png as well as .jpg files.\n");
    if(argc==4)
    {
        cv::Mat in=cv::imread(argv[1]);
        assert((in.cols&15)==0);
        assert((in.rows&15)==0);
        int s=(in.cols*in.rows*2);
        unsigned char *G2BR=new unsigned char[s];
        PMC::RGB2G2BR(in,G2BR);
        unsigned char *Out=new unsigned char[s*2];
        int il=in.rows*in.cols*3;
        cv::Mat G2BRM(in.rows*1.5f,in.cols,CV_8U,G2BR);
        int ol=PMC::Encode(G2BRM,atoi(argv[2]),Out);
        FILE* f=fopen(argv[3],"wb");
        fwrite(Out,1,ol,f);
        fclose(f);
    }
    if(argc==3)
    {
        FILE* f=fopen(argv[1],"rb");
        fseek(f,0,SEEK_END);
        int l=ftell(f);
        rewind(f);
        unsigned char *b=new unsigned char[l];
        fread(b,1,l,f);
        fclose(f);
        cv::Mat G2BRM=PMC::Decode(b,l);
        cv::Mat RGB=PMC::G2BR2RGB(G2BRM);
        cv::imwrite(argv[2],RGB);
    }
    return 0;
}

