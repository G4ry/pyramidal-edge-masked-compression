#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <iostream>
#include <string>
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

static cv::Mat FTD(cv::Mat &BW,const cv::Mat &BWS,int Threshold)
{
    int Mask[2]={0,0xff};
    cv::Mat Result;
    Result.create(BW.rows,BW.cols,CV_8U);
    int oy=0;
    int oyr=0;
    for(int y=0;y<BW.rows;y++)
    {
        for(int x=0;x<BW.cols;x++)
        {
            int c=BW.data[oy+x];
            int d=BWS.data[oy+x];
            int m=Mask[abs(c-d)>Threshold];
            Result.data[oyr+x]=((c>>1)|0x80)&m;
        }
        oy+=BW.step;
        oyr+=Result.step;
    }
    return Result;
}

static void FTD(cv::Mat &BW,const cv::Mat &BWM)
{
    assert(BW.cols==BWM.cols);
    assert(BW.rows==BWM.rows);
    int Mask[2]={0,0xff};
    int oy=0;
    for(int y=0;y<BW.rows;y++)
    {
        for(int x=0;x<BW.cols;x++)
        {
            int o=BW.data[oy+x];
            int c=BWM.data[oy+x];
            int m=Mask[c>>7];
            c=(c<<1)&m;o&=(m^0xff);
            BW.data[oy+x]=c|o;
        }
        oy+=BW.step;
    }
}

static cv::Mat Quarter(cv::Mat &BW)
{
    cv::Mat r;
    r.create(BW.rows>>1,BW.cols>>1,CV_8U);
    int oyb=0;
    int oyr=0;
    for(int y=0;y<r.rows;y++)
    {
        for(int x=0;x<r.cols;x++)
        {
            r.data[oyr+x]=BW.data[oyb+(x<<1)];
        }
        oyb+=(BW.step<<1);
        oyr+=r.step;
    }
    return r;
}

int main(int argc, char** argv)
{
    printf("rawpmc  G.Symons 2015\n");
    printf("Usage:\n");
    printf("Graphically show conversion of jpg file to raw pmc levels in png format:"
           "rawpmc infile.jpg  [Thresholds concatenated 01-09's] outfile\n");
    printf("Examples:\n");
    printf("rawpmc test.jpg 010203 test will use a 3 level pyramid with thresholds 1 for lowest res in pyramid, 2 for mid, and 3 for highest.\n");
    printf("Outputs original unencoded grey scale, grey scales of pmc encoded pyramid levels, and decoded lossy result:\n"
           "test-bw.png test-l0-3.png test-l1-2.png test-l2-1.png and base lowest scale test-l3-0.png and lossy result test-d.png\n");
    printf("Other extensions such as .png can be used as well as .jpg for input file.\n");
    if(argc==4)
    {
        char s[1024];

        cv::Mat orig=cv::imread(argv[1]);
        cv::cvtColor(orig,orig,CV_BGR2GRAY);
        cv::Mat in,r[16];
        int rt[16];
        in=orig;
        sprintf(s,"%s-bw.png",argv[3]);
        imwrite(s,in);
        sprintf(s,"%s-bw.jpg",argv[3]);
        imwrite(s,in);

        int Thresholds=atoi(argv[2]),t=Thresholds,Level=0;
        while(t)
        {
            rt[Level]=t%100;
            r[Level]=in;
            t/=100;
            Level++;
            cv::resize(in,in,cv::Size(0,0),0.5f,0.5f,cv::INTER_AREA);
        }
        
        sprintf(s,"%s-l%d-%d.png",argv[3],Level,0);
        imwrite(s,in);
        
        while(Level)
        {
            cv::resize(in,in,cv::Size(0,0),2,2,cv::INTER_LINEAR);
            Level--;
            r[Level]=FTD(r[Level],in,rt[Level]);
            sprintf(s,"%s-l%d-%d.png",argv[3],Level,rt[Level]);
            imwrite(s,r[Level]);
            FTD(in,r[Level]);
        }
        
        sprintf(s,"%s-d.png",argv[3]);
        imwrite(s,in);
    }
    return 0;
}

