#ifndef _PMC_H_
#define _PMC_H_
/*
 Pyramidal Masked Compression
 Copyright (c) 2015 G.Symons
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
struct PMC
{
    static int Encode(cv::Mat &G2BR,int Thresholds,void *OutBuffer,cv::Mat *PrevG2BR=0,int DeltaFrameThreshold=1);
    static cv::Mat Decode(void *InBuffer,int InLen,cv::Mat *PrevG2BR=0);
    static void RGB2G2BR(cv::Mat &RGB,unsigned char *G2BR);
    static cv::Mat G2BR2RGB(cv::Mat &G2BRM);
};
#endif


