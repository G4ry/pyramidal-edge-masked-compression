#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <iostream>
#include <string>
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "pmc.h"

#ifndef IRT_DEBU
#define IRT_DEBUG
#endif

#ifdef IRT_DEBUG
void IRTODS(const char *DebugString,...);
#define IRTAssert(exp)  if (!(exp)) {IRTODS( "%s:%d\n",__FILE__, __LINE__ );while(1);}
#else
inline void IRTODS(const char *DebugString,...){};
#define IRTAssert(exp)
#endif
#ifdef IRT_DEBUG
void IRTODS(const char *DebugString,...)
{
	va_list arg;
	va_start(arg, DebugString);
	static char str[1024];
	vsnprintf(str, 1024, DebugString, arg);
	va_end(arg);
	fprintf(stderr,"%s",str);
}
#endif

unsigned int IRTMSec(void)
{
	timeval tNow;
	gettimeofday(&tNow, NULL);
	return ((unsigned int)(tNow.tv_sec*1000+tNow.tv_usec/1000));
}

double IRTUSec(void)
{
	timeval tNow;
	gettimeofday(&tNow, NULL);
	return (double(tNow.tv_sec*1000000+tNow.tv_usec));
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}


int FullWrite(int sck,void *v,int s,int ps=0)
{
    const unsigned char *b=(unsigned char *)v;
    if(ps<=0) ps=s;
    while(s>0)
    {
        int l=s>ps?ps:s;
        int n=write(sck,b,l);
        if(n<0) return n;
        b+=n;
        s-=n;
    }
    return s;
}

int FullRead(int sck,void *v,int s,int ps=0)
{
    unsigned char *b=(unsigned char *)v;
    if(ps<=0) ps=s;
    while(s>0)
    {
        int l=s>ps?ps:s;
        int n=read(sck,b,l);
        if(n<0) return n;
        b+=n;
        s-=n;
    }
    return s;
}

int Verbose=1;

int client(const char *address,unsigned int portno,int Thresholds,int DeltaMod,int DeltaThreshold)
{
    int sockfd,n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    error("ERROR opening socket");
    server = gethostbyname(address);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    error("ERROR connecting");
    int flag = 1;
    setsockopt(sockfd,IPPROTO_TCP,TCP_NODELAY,(char *) &flag,sizeof(int));
    int Frames=-1;
    cv::VideoCapture cap;
    cap.open(0);
    if(!cap.isOpened()) return -1;
    cv::Mat CVideo,PrevG2BRM;
    unsigned char *G2BR=0,*Out=0;
    std::stringstream vtitle;
    int ack=0,tms;
    while(1)
    {
        cap >> CVideo;
        
        float sr=float(720&(~15))/CVideo.cols;
        cv::resize(CVideo,CVideo,cv::Size(sr*CVideo.cols,(int(sr*CVideo.rows)&(~31))),0,0,cv::INTER_NEAREST);
        
        cv::Mat Video=CVideo;
        
        int s=(Video.cols*Video.rows*2);
        
        if(!G2BR) G2BR=new unsigned char[s];
        PMC::RGB2G2BR(Video,G2BR);
        
        if(!Out) Out=new unsigned char[s*2];
        int il=Video.rows*Video.cols*3,ol;
        cv::Mat G2BRM(Video.rows*1.5f,Video.cols,CV_8U,G2BR);
        ol=PMC::Encode(G2BRM,Thresholds,Out,PrevG2BRM.empty()?0:&PrevG2BRM,DeltaThreshold);
        if(Frames<0) tms=IRTMSec();
        if(DeltaMod)
        {
            int m=DeltaMod+1;
            if(Frames%m) G2BRM.copyTo(PrevG2BRM); else PrevG2BRM=cv::Mat();
            if(Verbose)
            {
                if((Frames%m)==1) IRTODS("    \r");
                IRTODS("D:%d-->%d %f%% ",il,ol,ol*100.0f/il);
            }
            if(!(Frames%m))
            {
                int dtms=(IRTMSec()-tms);
                if(dtms<1) dtms=1;
                IRTODS(" %d FPS     ",(Frames*1000)/dtms);
            }
        }
        else
        if(Verbose)
        {
            if(Frames<0) tms=IRTMSec();
            int dtms=(IRTMSec()-tms);
            if(dtms<1) dtms=1;	
            IRTODS("\rF:%d-->%d %f%% %d FPS     ",il,ol,ol*100.0f/il,(Frames*1000)/dtms);
        }
        int n=FullWrite(sockfd,&ol,sizeof(ol));
        if(n<0) {IRTODS("Frame write error\n");exit(1);}
        n=FullWrite(sockfd,Out,ol);
        if(n<0) {IRTODS("G2BR write error:%d %d",n,s);exit(1);}
        int c=cv::waitKey(1);
        if(c=='q') break;
        
        
        n=FullRead(sockfd,&ack,sizeof(ack));
        if(n<0) {IRTODS("ACK read error:%d",n);exit(1);}
        Frames++;
    }
    delete [] Out;
    delete [] G2BR;
    return 0;
}

int server(unsigned int portno)
{
    int sockfd, newsockfd,n;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    
    sockfd=socket(AF_INET,SOCK_STREAM,0);
    if(sockfd<0)error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr))<0) error("ERROR on binding");
    listen(sockfd,5);
    clilen=sizeof(cli_addr);
    newsockfd=accept(sockfd,(struct sockaddr *) &cli_addr,&clilen);
    if(newsockfd<0)error ("ERROR on accept");
    int flag = 1;
    setsockopt(newsockfd,IPPROTO_TCP,TCP_NODELAY,(char *) &flag,sizeof(int));
    
    int Frames=-1,BufferSize=0;
    unsigned char *Buffer=0;
    std::stringstream vtitle;
    int ack=0,tms;
    cv::Mat G2BRM,PrevG2BRM;
    while(1)
    {
        n=FullWrite(newsockfd,&ack,sizeof(ack));
        if(n<0) {IRTODS("ACK write error:%d",n);exit(1);}
        int il;
        n=FullRead(newsockfd,&il,sizeof(il));
        if(n<0) {IRTODS("Frame read error\n");exit(1);}
        if(il>BufferSize){delete [] Buffer;BufferSize=(il*2);Buffer=new unsigned char [BufferSize];}
        n=FullRead(newsockfd,Buffer,il);
        if(n<0) {IRTODS("Buffer read error:%d %d",n,il);exit(1);}
        G2BRM=PMC::Decode(Buffer,il,PrevG2BRM.empty()?0:&PrevG2BRM);
        G2BRM.copyTo(PrevG2BRM);
        vtitle.str("");
        vtitle<<"G2BR To RGB:"<<G2BRM.cols<<"x"<<G2BRM.rows;
        cv::imshow(vtitle.str(),PMC::G2BR2RGB(G2BRM));
        if(Frames<0) tms=IRTMSec();
        if(Verbose)
        {
            int dtms=(IRTMSec()-tms);
            if(dtms<1) dtms=1;
            IRTODS("\r%d FPS     ",(Frames*1000)/dtms);
        }
        Frames++;
        int c=cv::waitKey(1);
        if(c=='q') break;
    }
    delete [] Buffer;
    close(newsockfd);
    close(sockfd);
    return 0;
}

int main(int argc, char** argv)
{
    IRTODS("Pyramidal Edge Masked Compression  G.Symons 2015\n");
    IRTODS("Usage:\n");
    IRTODS("Sever Capture  : pemc Port(Dec)\n"
           "  e.g. pemc 8080\n");
    IRTODS("Client Streamer: pemc Address(Named or dotted) Port(Dec) Fixed-Thresholds(concatenated 00-99) Deltas/Fixed(0-8) Delta-Threshold(0-9)\n"
           "  e.g. pemc 127.0.0.1 8080 0408 2 4 \n"
           "  Fixed-Frame thresholds 0408, 2 levels with threshold of 4 for high res level 0 of pyramid\n"
           "  2 Delta frames per fixed frame with threshold of 4 for delta frames.");
    IRTODS("When focused on windows use following keys in lower case:\n");
    IRTODS("q-:Quit.");
    if(argc==2) server(atoi(argv[1])); else if(argc==6) client(argv[1],atoi(argv[2]),atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));
    return 0;
}

