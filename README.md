Pyramidal Edge Detector Masked Lossy Video Compression Research by G. Symons 2015 G4ry@G4ry.org.uk


A highly efficient lossy compression algorithm that utilises a simple one absolute difference per pixel thresholded edge detection between scales of images to create sparse masks of the original image. Effectively acting as a frequency filter via the masking with quantising by the threshold of the edge detector. None of the complexity or overhead of block transform multiplications or VQ codebook generation.


Figure below: Top left is original image, diagonal is of pyramid scale edge detected masks (thresholded low 0, mid 04 and high 08) of original. Bottom right is image reconstructed from diagonal edge mask pyramid alone.

![screenshot](https://bitbucket.org/G4ry/pyramidal-edge-masked-compression/raw/master/ReadMeFigure.png)

A high resolution image has a lot of redundant information that is virtually the same at half the resolution, the same is true recursively. The highest resolution effectively has edges that are pertinent to that resolution only, the high frequency components. By pyramidally recursively masking the edges at different scales we have sparse images which are easily compressed. We can reconstruct the original image from the pyramid alone. By altering the threshold of the edge detector at different scales we can effect the lossy compression ratio. The lower the threshold the less compression.

The Unsharp Mask developed in the early part of the 20th century, which is a clever way of creating a high pass filter, still used to this day to sharpen images, was the inspiration behind this approach. As well as accelerated segment tested derivatives of Difference of Gaussian methods. Used extensively in computer vision, and even in a hidden sense in AI and machine learning, they have proved themselves as the workhorse of efficient robust statistical frequency analysis.

The differing quantisation of differing frequencies in the M/JPG standard is analogous to the scale dependent variable threshold value in the edge detector, where each scale, analogously, is like each discrete cosine wave in M/JPG DCT, the frequency filters. Unlike DCT there is no inefficient expensive matrix calculations with this solution, just per pixel operations, shifts, additions and comparisons, which are fast and can be vectorised, and also GPUs can be utilised in decoding the scaling and merging of textures in the pyramid.

Just in C++, even more so when utilising intrinsic vector instructions like SIMD and NEON, or GPUs, the performance should be better than JPG and MPG, with acceptable results relative to the algorithmic efficiency.

Pyramidal Masked Compression Copyright (c) 2015 G.Symons

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.