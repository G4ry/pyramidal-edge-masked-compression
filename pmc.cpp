/*
 Pyramidal Masked Compression
 Copyright (c) 2015 G.Symons
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "pmc.h"

struct FrameS
{
    enum {FrameID=0x64870001,MaxLevels=8,FlagDelta=1};
    unsigned int ID,Width[MaxLevels],Height[MaxLevels],Levels,Offset[MaxLevels],Size[MaxLevels],Flags;
    FrameS()
    {
        memset(this,0,sizeof(FrameS));
        ID=FrameID;
        Levels=0;
        Flags=0;
    }
    ~FrameS(){};
};

static cv::Mat FTD(cv::Mat &BW1,cv::Mat &BW2,int Threshold)
{
    int Mask[2]={0,0xff};
    cv::Mat Result;
    Result.create(BW1.rows,BW1.cols,CV_8U);
    int oy=0;
    int oyr=0;
    for(int y=0;y<BW1.rows;y++)
    {
        for(int x=0;x<BW1.cols;x++)
        {
            int c=BW1.data[oy+x];
            int d=BW2.data[oy+x];
            int m=Mask[abs(c-d)>Threshold];
            Result.data[oyr+x]=(c|0x80)&m;
        }
        oy+=BW1.step;
        oyr+=Result.step;
    }
    return Result;
}

static int ZeroOut(const unsigned char *In,int l,unsigned char *Out)
{
    int i=0,z=0,o=0,zl;
    while(i<l)
    {
        if(In[i])
            Out[o++]=In[i++]&0x7f;
        else
        {
            int z=i;
            while(!In[i]) i++;
            z=i-z;
            while(z>0)
            {
                if(z<128) zl=z; else zl=128;
                Out[o++]=(zl-1)|0x80;
                z-=zl;
            }
        }
    }
    return o;
}

static int ZeroIn(const unsigned char *In,int l,unsigned char *Out)
{
    int i=0,o=0;
    while(i<l)
    {
        if(!(In[i]&0x80))
            Out[o++]=In[i++];
        else
            while(In[i]&0x80) o+=(In[i++]&0x7f)+1;
    }
    return o;
}

void PMC::RGB2G2BR(cv::Mat &RGB,unsigned char *G2BR)
{
    cv::Mat &YUV=RGB;
    int s=0,w=RGB.cols,h=RGB.rows,wo2=w>>1,wh=RGB.cols*RGB.rows;
    int d=wh,jo=0,js=RGB.cols*2*3;
    for(int j=0;j<RGB.rows;j+=2)
    {
        int io=jo;
        for(int i=0;i<RGB.cols;i+=2)
        {
            G2BR[d]=(RGB.data[io+0]>>1);
            G2BR[d+wo2]=(RGB.data[io+2]>>1);
            d++;
            io+=2*3;
        }
        d+=wo2;
        jo+=js;
    }
    int sd=wh;
    int ds=0;
    for(int j=0;j<h;j++)
    {
        for(int i=0;i<w;i++)
        {
            G2BR[ds++]=(RGB.data[s+1]>>1);
            s+=3;
        }
        if(j&1) sd+=w;
    }
}

cv::Mat PMC::G2BR2RGB(cv::Mat &G2BRM)
{
    unsigned char *G2BR=G2BRM.data;
    cv::Mat RGB;
    RGB.create(G2BRM.rows*2.0f/3.0f,G2BRM.cols,CV_8UC3);
    cv::Mat &YUV=RGB;
    int s=0,w=RGB.cols,wo2=w>>1,wh=RGB.cols*RGB.rows;
    int d=wh,jo=0,js=RGB.cols*2*3,js1=RGB.cols*3;
    for(int j=0;j<RGB.rows;j+=2)
    {
        int io=jo;
        for(int i=0;i<RGB.cols;i+=2)
        {
            YUV.data[io+0]=YUV.data[io+3+0]=YUV.data[io+js1+0]=YUV.data[io+js1+3+0]=(G2BR[d]<<1);
            YUV.data[io+2]=YUV.data[io+3+2]=YUV.data[io+js1+2]=YUV.data[io+js1+3+2]=(G2BR[d+wo2]<<1);
            d++;
            io+=2*3;
        }
        d+=wo2;
        jo+=js;
    }
    for(int i=0;i<wh;i++)
    {
        YUV.data[s+1]=G2BR[i]<<1;
        s+=3;
    }
    return RGB;
}

int PMC::Encode(cv::Mat &G2BR,int Thresholds,void *OutBuffer,cv::Mat *PrevG2BR,int DeltaFrameThreshold)
{
    assert(!(G2BR.cols&15));
    assert(!(G2BR.rows&15));
    
    unsigned char *Out=(unsigned char *)OutBuffer;
    
    FrameS fs;

    int OutO=sizeof(fs),tl=0;
    
    if(PrevG2BR)
    {
        assert(PrevG2BR->cols==G2BR.cols);
        assert(PrevG2BR->rows==G2BR.rows);
        
        cv::Mat DG2BR=FTD(G2BR,*PrevG2BR,DeltaFrameThreshold);
        int Level=0;
        int il=DG2BR.cols*DG2BR.rows;
        int ol=ZeroOut(DG2BR.data,il,&Out[OutO]);
        fs.Width[Level]=DG2BR.cols;
        fs.Height[Level]=DG2BR.rows;
        fs.Offset[Level]=OutO;
        fs.Size[Level]=ol;
        fs.Levels=1;
        fs.Flags=fs.FlagDelta;
        tl+=il;
        OutO+=ol;
        memcpy(Out,&fs,sizeof(fs));
        return OutO;
    }
    
    int Level=0,Thresh=Thresholds;
    int rt[FrameS::MaxLevels];
    cv::Mat DYUV[FrameS::MaxLevels],FTDM[FrameS::MaxLevels];
    DYUV[0]=G2BR;
    while(true)
    {
        rt[Level]=Thresh%100;
        Thresh/=100;
        Level++;
        cv::resize(DYUV[Level-1],DYUV[Level],cv::Size(0,0),0.5f,0.5f,cv::INTER_NEAREST);
        if(!Thresh) break;
        if(Level>=(FrameS::MaxLevels-1)) break;
    }
    int il=DYUV[Level].cols*DYUV[Level].rows;
    memcpy(&Out[OutO],DYUV[Level].data,il);
    fs.Width[Level]=DYUV[Level].cols;
    fs.Height[Level]=DYUV[Level].rows;
    fs.Offset[Level]=OutO;
    fs.Size[Level]=il;
    OutO+=il;
    fs.Levels=Level+1;
    cv::Mat &Base=DYUV[Level];
    while(Level)
    {
        cv::resize(Base,Base,cv::Size(0,0),2,2,cv::INTER_LINEAR);
        Level--;
        int LevelThreshold=rt[Level];
        FTDM[Level]=FTD(DYUV[Level],Base,LevelThreshold);
        int il=FTDM[Level].cols*FTDM[Level].rows;
        int ol=ZeroOut(FTDM[Level].data,il,&Out[OutO]);
        fs.Width[Level]=DYUV[Level].cols;
        fs.Height[Level]=DYUV[Level].rows;
        fs.Offset[Level]=OutO;
        fs.Size[Level]=ol;
        ZeroIn(&Out[fs.Offset[Level]],fs.Size[Level],Base.data);
        tl+=il;
        OutO+=ol;
    }
    memcpy(Out,&fs,sizeof(fs));
    return OutO;
}

cv::Mat PMC::Decode(void *InBuffer,int InLen,cv::Mat *PrevG2BR)
{
    unsigned char *ib=(unsigned char *)InBuffer;
    
    cv::Mat G2BR;

    FrameS fs=*(FrameS *)ib;
    assert(fs.ID==FrameS::FrameID);
    
    if(fs.Flags&fs.FlagDelta)
    {
        assert(PrevG2BR);
        PrevG2BR->copyTo(G2BR);
        int ol=ZeroIn(&ib[fs.Offset[0]],fs.Size[0],G2BR.data);
        return G2BR;
    }

    int l=fs.Levels-1;

    G2BR.create(fs.Height[l],fs.Width[l],CV_8U);
    memcpy(G2BR.data,&ib[fs.Offset[l]],fs.Size[l]);

    while(l)
    {
        l--;
        cv::resize(G2BR,G2BR,cv::Size(0,0),2,2,cv::INTER_LINEAR);
        int ol=ZeroIn(&ib[fs.Offset[l]],fs.Size[l],G2BR.data);
    }

    return G2BR;
}

